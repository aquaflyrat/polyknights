#ifndef MASTER_H_INCLUDED
#define MASTER_H_INCLUDED

#include "lib_sdl.h"
#include "lib_sdlimg.h"
#include "lib_sdlttf.h"
#include "lib_sdlnet.h"
#include "lib_gl.h"
#include "lib_al.h"
#include "lib_zlib.h"
#include "lib_ogg.h"
#include "lib_vorbis.h"
#include "audio.h"
#include "exceptions.h"
#include "graphics.h"
#include "input.h"
#include "input_enums.h"
#include "math.h"
#include "network.h"
#include "os.h"
#include "system.h"
#include "utils.h"
#include "window.h"

#include "game.h"
#include "sound.h"
#include "render.h"

#endif