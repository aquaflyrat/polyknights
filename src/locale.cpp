#include "locale.h"

#include <fstream>

namespace Locale
{
    static bool english = 1;

    bool English() {return english;}
    bool Russian() {return !english;}
    void UseEnglish() {english = 1; SaveToFile();}
    void UseRussian() {english = 0; SaveToFile();}

    static constexpr const char *config_file_name = "locale.cfg";

    void SaveToFile()
    {
        std::ofstream out(config_file_name);
        if (!out)
            return;
        out << (English() ? "en" : "ru");
    }

    void LoadFromFile()
    {
        std::ifstream in(config_file_name);
        if (!in)
        {
            UseEnglish();
            return;
        }
        std::string str;
        in >> str;
        if (str == "en")
            english = 1;
        else if (str == "ru")
            english = 0;
        else
            UseEnglish();
    }
}