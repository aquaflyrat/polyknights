#ifndef KEYS_H_INCLUDED
#define KEYS_H_INCLUDED

#include "input.h"

namespace Keys
{
    #define KEY_LIST \
        K( left  , Input::Key<'A'>()) \
        K( right , Input::Key<'D'>()) \
        K( jump  , Input::Key<'W'>()) \

    #define K(name, value) Input::KeyID name();
    namespace Codes {KEY_LIST}
    #undef K
    #define K(name, value) bool name();
    namespace Down     {KEY_LIST}
    namespace Pressed  {KEY_LIST}
    namespace Released {KEY_LIST}
    #undef K
    #define K(name, value) const char *name();
    namespace Names {KEY_LIST}
    #undef K

    int Count();
    Input::KeyID CodeAt(int pos);
    void SetCodeAt(int pos, Input::KeyID code);
    const char *NameAt(int pos);

    bool *Dupes();
    bool AnyDupes();

    void LoadFromFile();
    void ResetToDefault();
}

#endif