#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "math.h"
#include "utils.h"
#include <random>

namespace Game
{
    extern std::mt19937 rng;
    extern ivec2 win_min, win_max;
    extern Utils::TickStabilizer *ts;
}

#endif