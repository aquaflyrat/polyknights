#include "render.h"

namespace Render
{
    static constexpr int render_queue_size = 0x30000;

    namespace ShaderSources
    {
        #define VERTEX_HEADER ForWindows("#version 330 compatibility") \
                              ForMac    ("#version 150") \
                              ForMobile ("#version 100")
        #define FRAGMENT_HEADER VERTEX_HEADER ForMobile("\nprecision mediump float;")
        inline namespace Main
        {
            Graphics::ShaderSource main
            {
{"a_pos","a_texpos","a_color","a_factors"},
{"u_matrix","u_texture"},
VERTEX_HEADER R"(
uniform mat4 u_matrix;
attribute vec2 a_pos;
attribute vec2 a_texpos;
attribute vec4 a_color;
attribute vec2 a_factors;
varying vec4 v_color;
varying vec2 v_texpos;
varying vec2 v_factors;
void main()
{
    gl_Position = u_matrix * vec4(a_pos, 0, 1);
    v_color   = a_color;
    v_texpos  = a_texpos / 1024.;
    v_factors = a_factors;
})",
FRAGMENT_HEADER R"(
uniform sampler2D u_texture;
varying vec4 v_color;
varying vec2 v_texpos;
varying vec2 v_factors;
void main()
{
    gl_FragColor = v_color * (1. - v_factors).xxxy + texture2D(u_texture, v_texpos) * v_factors.xxxy;
    gl_FragColor.rgb *= gl_FragColor.a;
})"
            };
        }

        inline namespace Menu
        {
            Graphics::ShaderSource menu
            {
{"a_pos"},
{"u_time","u_size"},
VERTEX_HEADER R"(
attribute vec2 a_pos;
void main()
{
    gl_Position = vec4(a_pos, 0, 1);
})",
FRAGMENT_HEADER R"(
uniform float u_time;
uniform vec2 u_size;
void main()
{
    float pi = atan(1.) * 4, range = min(u_size.x, u_size.y) / 5.0;
    float angle = u_time / 50.0 * pi;
    float cur_angle = atan(gl_FragCoord.x - u_size.x/2., gl_FragCoord.y - u_size.y/2.) - angle + 4*pi;
    gl_FragColor.a = 1;
    const vec3 color_a = vec3(1,5./9.,0),
               color_b = vec3(3./4.,1,0),
               color_c = vec3(1,.5,1);
    gl_FragColor.rgb = color_a * max(0.0, (1.0 - abs(mod(cur_angle, 2*pi)-pi) / pi * 3./2.)) +
                       color_b * max(0.0, (1.0 - abs(mod(cur_angle+pi*2./3., 2*pi)-pi) / pi * 3./2.)) +
                       color_c * max(0.0, (1.0 - abs(mod(cur_angle-pi*2./3., 2*pi)-pi) / pi * 3./2.));
    vec2 a1 = u_size/2.0 + vec2(0, range);
    vec2 a2 = u_size/2.0 + vec2(sin(pi*2./3.)*range, cos(pi*2./3.)*range);
    vec2 a3 = u_size/2.0 + vec2(sin(-pi*2./3.)*range, cos(-pi*2./3.)*range);
    float side = length(a2-a1);
    float d1 = 1 - dot(a2-a1, gl_FragCoord.xy-a1) / side / side;
    float d2 = 1 - dot(a3-a2, gl_FragCoord.xy-a2) / side / side;
    float d3 = 1 - dot(a1-a3, gl_FragCoord.xy-a3) / side / side;
    float d = max(d1, max(d2, d3));
    float dd = min(d1, min(d2, d3)) * 2;
    float factor = dd * (1.-d);
    factor -= 0.6;
    /*if (factor < 1.)
    {
        gl_FragColor.rgb = vec3(0,0,0);
    }
    else*/
        gl_FragColor.rgb *= factor;
})"
            };
        }

        inline namespace Map
        {
            Graphics::ShaderSource map
            {
{"a_pos","a_color","a_norm_a","a_norm_b","a_norm_c"},
{"u_proj_mat","u_view_mat"},
VERTEX_HEADER R"(
uniform mat4 u_proj_mat;
uniform mat4 u_view_mat;
attribute vec2 a_pos;
attribute vec3 a_color;
attribute vec4 a_norm_a;
attribute vec4 a_norm_b;
attribute vec4 a_norm_c;
varying vec3 v_color;
varying vec4 v_norm_a;
varying vec4 v_norm_b;
varying vec4 v_norm_c;
void main()
{
    mat4 mat = u_proj_mat * u_view_mat;
    mat4 norm_mat = mat4(mat3(u_view_mat));
    gl_Position = mat * vec4(a_pos,0,1);
    v_color = a_color;
    v_norm_a = norm_mat * a_norm_a;
    v_norm_b = norm_mat * a_norm_b;
    v_norm_c = norm_mat * a_norm_c;
})",
FRAGMENT_HEADER R"(
varying vec3 v_color;
varying vec4 v_norm_a;
varying vec4 v_norm_b;
varying vec4 v_norm_c;
void main()
{
    //float factor = sqrt(v_dist.x * v_dist.y * v_dist.z / 0.125) * 0.5 + 0.5;
    //gl_FragColor = vec4(v_color * factor,1);
    int nearest;
    if (v_norm_a.w < v_norm_b.w)
        if (v_norm_a.w < v_norm_c.w)
            nearest = 0;
        else
            nearest = 2;
    else
        if (v_norm_b.w < v_norm_c.w)
            nearest = 1;
        else
            nearest = 2;

    vec4 true_normal;
    switch (nearest)
    {
      case 0:
        true_normal = v_norm_a;
        break;
      case 1:
        true_normal = v_norm_b;
        break;
      case 2:
        true_normal = v_norm_c;
        break;
    }
    true_normal.xyz /= true_normal.w;
    true_normal.xy *= pow(0.4 - true_normal.w,1.5);
    true_normal.xyz = normalize(true_normal.xyz);
    vec3 light_dir = vec3(0,1,0);
    float light = dot(-light_dir, true_normal.xyz) * .8 + .6;
    gl_FragColor = vec4(v_color * light, 1);
})"
            };
        }

        inline namespace World
        {
            Graphics::ShaderSource world
            {
{"a_pos","a_texpos","a_color","a_factors"},
{"u_proj_mat","u_view_mat","u_texture"},
VERTEX_HEADER R"(
uniform mat4 u_proj_mat;
uniform mat4 u_view_mat;
attribute vec2 a_pos;
attribute vec2 a_texpos;
attribute vec4 a_color;
attribute vec2 a_factors;
varying vec4 v_color;
varying vec2 v_texpos;
varying vec2 v_factors;
void main()
{
    gl_Position = u_proj_mat * u_view_mat * vec4(a_pos, 0, 1);
    v_color   = a_color;
    v_texpos  = a_texpos / 1024.;
    v_factors = a_factors;
})",
FRAGMENT_HEADER R"(
uniform sampler2D u_texture;
varying vec4 v_color;
varying vec2 v_texpos;
varying vec2 v_factors;
void main()
{
    gl_FragColor = v_color * (1. - v_factors).xxxy + texture2D(u_texture, v_texpos) * v_factors.xxxy;
    gl_FragColor.rgb *= gl_FragColor.a;
})"
            };
        }
        #undef VERTEX_HEADER
        #undef FRAGMENT_HEADER
    }

    Graphics::Shader *main_shader,
                     *menu_shader,
                     *map_shader,
                     *world_shader;

    Graphics::Texture *main_texture;

    Graphics::FontData main_font_data;

    static Graphics::Font *main_font;

    Graphics::RenderQueue<Layouts::Main> *main_queue;

    void Initialize()
    {
        ExecuteThisOnce();

        Graphics::Blend::Enable();
        Graphics::Blend::Presets::FuncNormal_Pre();

        main_shader  = new Graphics::Shader("Main" , ShaderSources::main);
        menu_shader  = new Graphics::Shader("Menu" , ShaderSources::menu);
        map_shader   = new Graphics::Shader("Map"  , ShaderSources::map );
        world_shader = new Graphics::Shader("World", ShaderSources::world);

        main_font = new Graphics::Font("assets/font.ttf", 39);
        auto tex_data = Graphics::ImageData::FromPNG("assets/texture.png");
        main_font->RenderGlyphs(main_font_data, tex_data, {0,0}, {256,512}, {Utils::Encodings::cp1251(), 256});
        main_texture = new Graphics::Texture(tex_data);
        main_texture->LinearInterpolation(1);
        main_shader->SetUniform<Graphics::Texture>(1, *main_texture);
        world_shader->SetUniform<Graphics::Texture>(2, *main_texture);

        main_queue = new Graphics::RenderQueue<Layouts::Main>(render_queue_size);
    }
}