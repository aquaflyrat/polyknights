#include "keys.h"

#include <fstream>

namespace Keys
{
    static constexpr const char *config_file_name = "controls.cfg";

    namespace Codes
    {
        namespace Values
        {
            #define K(name, value) Input::KeyID name = value;
            KEY_LIST
            #undef K
        }

        #define K(name, value) Input::KeyID name() {return Values::name;}
        KEY_LIST
        #undef K
    }

    #define K(name, value) 0,
    static bool dupe_array[]{KEY_LIST};
    #undef K

    static void UpdateDupes()
    {
        for (int i = 0; i < Count(); i++)
        {
            dupe_array[i] = 0;
            for (int j = 0; j < Count(); j++)
            {
                if (i == j)
                    continue;
                if (CodeAt(i) == CodeAt(j))
                {
                    dupe_array[i] = 1;
                    break;
                }
            }
        }
    }

    static void SaveToFile()
    {
        std::ofstream out(config_file_name);
        if (!out)
            return;
        #define K(name, value) out << Codes::Values::name << '\n';
        KEY_LIST
        #undef K
    }

    void ResetToDefault()
    {
        #define K(name, value) Codes::Values::name = value;
        KEY_LIST
        #undef K
        UpdateDupes();
        SaveToFile();
    }

    void LoadFromFile()
    {
        std::ifstream in(config_file_name);
        if (!in)
        {
            SaveToFile();
            return;
        }
        #define K(name, value) in >> Codes::Values::name;
        KEY_LIST
        #undef K
        if (!in)
            ResetToDefault();
    }

    namespace Down
    {
        #define K(name, value) bool name() {return Input::KeyDown(Codes::Values::name);}
        KEY_LIST
        #undef K
    }
    namespace Pressed
    {
        #define K(name, value) bool name() {return Input::KeyPressed(Codes::Values::name);}
        KEY_LIST
        #undef K
    }
    namespace Released
    {
        #define K(name, value) bool name() {return Input::KeyReleased(Codes::Values::name);}
        KEY_LIST
        #undef K
    }

    namespace Names
    {
        #define K(name, value) const char *name() \
            { \
                const char *ret = Input::KeyName(Codes::Values::name); \
                if (!ret || !*ret) \
                    return Jo('<', Codes::Values::name, '>'); \
                else \
                    return ret; \
            }
        KEY_LIST
        #undef K
    }

    int Count()
    {
        #define K(name, value) 0,
        using char_array = char[];
        return int(sizeof char_array{KEY_LIST});
        #undef K
    }

    Input::KeyID CodeAt(int pos)
    {
        #define K(name, value) &Codes::Values::name,
        static constexpr Input::KeyID *arr[] {KEY_LIST};
        #undef K
        return *arr[pos];
    }

    void SetCodeAt(int pos, Input::KeyID code)
    {
        #define K(name, value) &Codes::Values::name,
        static constexpr Input::KeyID *arr[] {KEY_LIST};
        #undef K
        *arr[pos] = code;
        UpdateDupes();
        if (!AnyDupes())
            SaveToFile();
    }

    const char *NameAt(int pos)
    {
        const char *ret = Input::KeyName(CodeAt(pos));
        if (!ret || !*ret)
            return Jo('<', CodeAt(pos), '>');
        else
            return ret;
    }

    bool *Dupes()
    {
        return dupe_array;
    }
    bool AnyDupes()
    {
        for (auto it : dupe_array)
            if (it)
                return 1;
        return 0;
    }
}