#include "state_menu.h"
#include "state_game.h"
#include "master.h"
#include "locale.h"
#include "keys.h"

namespace States
{
    void Menu()
    {
        Render::main_texture->LinearInterpolation(0);
        static constexpr fvec4 button_color(1,1,0.7,1),
                               button_active_color(1,1,1,1),
                               button_shadow_color(0.2,0.2,0.2,1),
                               button_controls_key(0.6,0.6,0.6,1),
                               button_controls_active_key(0.8,0.8,0.8,1),
                               button_controls_selected_key(0,0.6,0,1),
                               button_controls_selected_key_shadow(0,0.12,0,1),
                               button_controls_dupe_key(0.6,0,0,1),
                               button_controls_dupe_key_shadow(0.12,0,0,1);

        struct MenuData
        {
            Utils::Array<const char *(*)()> buttons;
        };

        const MenuData main_menu{{Locale::menu_singleplayer,
                                  Locale::menu_multiplayer,
                                  Locale::menu_settings,
                                  Locale::menu_quit}};
        const MenuData settings_menu{{Locale::menu_language,
                                      Locale::menu_controls,
                                      Locale::menu_back}};
        const MenuData controls_menu{{
                                      #define K(name, value) Locale::keys_##name,
                                      KEY_LIST
                                      #undef K
                                      Locale::menu_reset_controls,
                                      Locale::menu_back}};
        const MenuData network_menu{{Locale::menu_connect,
                                     Locale::menu_host,
                                     Locale::menu_back}};
        const MenuData connect_menu{{Locale::menu_ip,
                                     Locale::menu_join,
                                     Locale::menu_back}};

        static constexpr float menu_y_button_spacing = 0.08;

        static const float text_space_width = Render::GetTextWidth(" ");

        int current_button = -1;

        bool fade_in_progress = 0;
        float fade_timer = 0;

        bool button_click_anim_started = 0;
        float button_click_anim_timer = 0;
        float state_load_fade = 1;
        float button_creation_timer = 1;
        bool swap_language_at_anim_end = 0;

        bool controls_key_selected = 0;
        static const Utils::Array<Input::KeyID> controls_disabled_buttons({Input::Key_Esc()});

        int backspace_hold_timer = 0;

        static std::string ip_string = "";

        const MenuData *current_menu = &main_menu, *next_menu = 0;

        auto YOffset = [&]() -> int
        {
            if (current_menu != &main_menu)
                return -menu_y_button_spacing * Window::Size().y * current_menu->buttons.Size() / 2 + Render::main_font_data.Height();
            else
                return 0;
        };

        auto Tick = [&]()
        {
            if (!fade_in_progress && !button_click_anim_started && !controls_key_selected)
            {
                current_button = -1;
                for (int i = 0; i < int(current_menu->buttons.Size()); i++)
                {
                    int width = Render::GetTextWidth(current_menu->buttons[i]());
                    int xpos;
                    if (current_menu == &controls_menu && i < int(current_menu->buttons.Size()) - 2)
                    {
                        xpos = -width + text_space_width/2;
                        width += Render::GetTextWidth(Keys::NameAt(i));
                    }
                    else
                        xpos = -width/2;
                    if (Input::MouseInRect({xpos, std::lround(YOffset() + Window::Size().y * menu_y_button_spacing * i) - Render::main_font_data.Ascent()}, {width, Render::main_font_data.Height()}))
                    {
                        current_button = i;
                        break;
                    }
                }

                if (current_menu == &connect_menu)
                {
                    if (current_button == 0)
                        current_button = -1;
                }
            }

            if (fade_in_progress)
            {
                fade_timer += 0.01;

                if (fade_timer > 1)
                {
                    if (current_menu == &main_menu)
                    {
                        switch (current_button)
                        {
                          case 0: // Singleplayer.
                            Sys::SetCurrentFunction(States::Game);
                            break;
                          case 3: // Quit.
                            Sys::Exit();
                            break;
                        }
                    }
                    else if (current_menu == &network_menu)
                    {
                        switch (current_button)
                        {
                          case 1:
                            Sys::SetCurrentFunction(States::Game);
                            break;
                        }
                    }
                    else if (current_menu == &connect_menu)
                    {
                        switch (current_button)
                        {
                          case 1:
                            Sys::SetCurrentFunction(States::Game);
                            break;
                        }
                    }
                }
            }
            if (button_click_anim_started)
            {
                if (button_click_anim_timer * 3 < 1)
                    button_click_anim_timer = (button_click_anim_timer + 0.01) * 1.05;
                else
                {
                    if (swap_language_at_anim_end)
                    {
                        swap_language_at_anim_end = 0;
                        if (Locale::English())
                            Locale::UseRussian();
                        else
                            Locale::UseEnglish();
                    }
                    if (next_menu)
                    {
                        button_click_anim_timer = 0;
                        button_click_anim_started = 0;
                        current_menu = next_menu;
                        next_menu = 0;
                        button_creation_timer = 1;
                        current_button = -1;
                    }
                }
            }

            if (state_load_fade > 0)
            {
                state_load_fade -= 0.03;
                if (state_load_fade < 0)
                    state_load_fade = 0;
            }
            if (button_creation_timer > 0)
            {
                button_creation_timer -= 0.03;
                if (button_creation_timer < 0)
                    button_creation_timer = 0;
            }

            if (controls_key_selected) // Setting controls.
            {
                Input::KeyID cur_key = Input::AnyKeyPressed();
                if (cur_key != 0)
                {
                    bool ok = 1;
                    for (Input::KeyID it : controls_disabled_buttons)
                    {
                        if (it == cur_key)
                        {
                            ok = 0;
                            break;
                        }
                    }

                    if (ok)
                    {
                        controls_key_selected = 0;
                        Keys::SetCodeAt(current_button, cur_key);
                        if (Keys::AnyDupes())
                            Sound::click_error_r();
                        else
                            Sound::click_ok_r();
                    }
                    else
                        Sound::click_error_r();
                }
            }

            // Typing IP.
            if (current_menu == &connect_menu)
            {
                if ((Input::KeyDown(Input::Key_Ctrl_L()) || Input::KeyDown(Input::Key_Ctrl_R())) && Input::KeyPressed(Input::Key<'v'>()))
                {
                    ip_string = "";
                    char *txt = SDL_GetClipboardText();
                    const char *ptr = txt;
                    while (*ptr && ip_string.size() < 15)
                    {
                        if (*ptr == '.' || (*ptr >= '0' && *ptr <= '9'))
                            ip_string += *ptr;
                        ptr++;
                    }
                    SDL_free(txt);
                }

                if (Input::KeyDown(Input::Key_Backspace()))
                    backspace_hold_timer++;
                else
                    backspace_hold_timer = 0;
                if (backspace_hold_timer == 1 || (backspace_hold_timer > 30 && backspace_hold_timer % 5 == 0))
                    if (ip_string.size() > 0)
                        ip_string.resize(ip_string.size() - 1);

                int id;
                switch (Input::AnyKeyPressed())
                {
                    case Input::Key<'0'>():
                    case Input::Key_Num<'0'>(): id = 0; break;
                    case Input::Key<'1'>():
                    case Input::Key_Num<'1'>(): id = 1; break;
                    case Input::Key<'2'>():
                    case Input::Key_Num<'2'>(): id = 2; break;
                    case Input::Key<'3'>():
                    case Input::Key_Num<'3'>(): id = 3; break;
                    case Input::Key<'4'>():
                    case Input::Key_Num<'4'>(): id = 4; break;
                    case Input::Key<'5'>():
                    case Input::Key_Num<'5'>(): id = 5; break;
                    case Input::Key<'6'>():
                    case Input::Key_Num<'6'>(): id = 6; break;
                    case Input::Key<'7'>():
                    case Input::Key_Num<'7'>(): id = 7; break;
                    case Input::Key<'8'>():
                    case Input::Key_Num<'8'>(): id = 8; break;
                    case Input::Key<'9'>():
                    case Input::Key_Num<'9'>(): id = 9; break;
                    case Input::Key<','>():
                    case Input::Key<'.'>():
                    case Input::Key<'/'>():
                    case Input::Key_Num<'.'>(): id = 10; break;
                    default: id = -1; break;
                }
                if (id != -1 && ip_string.size() < 15)
                {
                    if (id == 10)
                        ip_string += '.';
                    else
                        ip_string += '0' + id;
                }
            }

            if (Input::MouseButtonPressed(1) && !fade_in_progress && !button_click_anim_started && current_button != -1 && !controls_key_selected)
            {
                bool click_sound = 1;
                if (current_menu == &main_menu)
                {
                    button_click_anim_timer = 0;
                    button_click_anim_started = 1;
                    if (current_button == 2)
                    {
                        next_menu = &settings_menu;
                    }
                    else if (current_button == 1)
                    {
                        next_menu = &network_menu;
                    }
                    else
                    {
                        fade_timer = 0;
                        fade_in_progress = 1;
                    }
                }
                else if (current_menu == &network_menu)
                {
                    button_click_anim_timer = 0;
                    button_click_anim_started = 1;
                    if (current_button == 0)
                    {
                        next_menu = &connect_menu;
                    }
                    else if (current_button == 2)
                    {
                        next_menu = &main_menu;
                    }
                    else
                    {
                        fade_timer = 0;
                        fade_in_progress = 1;
                    }
                }
                else if (current_menu == &connect_menu)
                {
                    button_click_anim_timer = 0;
                    button_click_anim_started = 1;
                    if (current_button == 2)
                    {
                        next_menu = &main_menu;
                    }
                    else
                    {
                        fade_timer = 0;
                        fade_in_progress = 1;
                    }
                }
                else if (current_menu == &settings_menu)
                {
                    button_click_anim_timer = 0;
                    button_click_anim_started = 1;
                    if (current_button == 0) // Language selection.
                    {
                        swap_language_at_anim_end = 1;
                        next_menu = &settings_menu;
                    }
                    else if (current_button == 1) // Controls.
                    {
                        next_menu = &controls_menu;
                    }
                    else if (current_button == int(current_menu->buttons.Size()) - 1)
                    {
                        next_menu = &main_menu;
                    }
                    else
                    {
                        fade_timer = 0;
                        fade_in_progress = 1;
                    }
                }
                else if (current_menu == &controls_menu)
                {
                    if (current_button == int(current_menu->buttons.Size()) - 1)
                    {
                        if (!Keys::AnyDupes())
                        {
                            button_click_anim_timer = 0;
                            button_click_anim_started = 1;
                            next_menu = &settings_menu;
                        }
                        else
                            click_sound = 0;
                    }
                    else if (current_button == int(current_menu->buttons.Size()) - 2)
                    {
                        Keys::ResetToDefault();
                    }
                    else
                        controls_key_selected = 1;
                }
                if (click_sound)
                    Sound::click_r();
            }
        };
        auto Render = [&]()
        {
            Render::menu_shader->Use();
            Render::MenuBackground();
            Render::main_shader->Use();
            Render::main_queue->Clear();

            float spacing = -smoothstep(button_creation_timer) * 2;

            for (int i = 0; i < int(current_menu->buttons.Size()); i++)
            {
                float alpha = (button_click_anim_started ? std::min(1-button_click_anim_timer * (i == current_button ? 3 : 7),1.f) : 1) * (1 - button_creation_timer);
                int yoffset = std::lround((button_click_anim_started ? sign(i-current_button) * button_click_anim_timer * 75 : 0) + YOffset());
                int xoffset = 0;
                int alignment = 0;
                int ypos = std::lround(Window::Size().y * menu_y_button_spacing * i + yoffset);
                if (current_menu == &controls_menu && i < int(current_menu->buttons.Size()) - 2)
                {
                    alignment = 1;
                    xoffset = text_space_width / 2;
                    bool hlight = (controls_key_selected && i == current_button);
                    fvec4 color = button_controls_key,
                          shadow_color = button_shadow_color;

                    if (i == current_button)
                    {
                        color = button_controls_active_key;
                        shadow_color = button_controls_selected_key_shadow;
                    }
                    if (Keys::Dupes()[i])
                    {
                        color = button_controls_dupe_key;
                        shadow_color = button_controls_dupe_key_shadow;
                    }
                    if (hlight)
                    {
                        color = button_controls_selected_key;
                        shadow_color = button_controls_selected_key_shadow;
                    }
                    Render::Text({float(xoffset), float(ypos)}, -1, color.change_a(alpha), {1,2}, shadow_color.change_a(alpha), Keys::NameAt(i), spacing);
                }

                fvec4 color = (i == current_button ? button_active_color : button_color);
                fvec4 shadow_color = button_shadow_color;

                if (current_menu == &connect_menu && i == 0)
                {
                    color = button_controls_key;
                    xoffset -= Render::GetTextWidth(ip_string.c_str(), spacing) / 2;
                    Render::Text({Render::GetTextWidth(current_menu->buttons[i](), spacing)/2, float(ypos)}, alignment, button_active_color.change_a(alpha), {1,2}, shadow_color.change_a(alpha), ip_string.c_str(), spacing);
                    if (Sys::TickCounter() % 60 < 30)
                        Render::Text({Render::GetTextWidth(current_menu->buttons[i](), spacing)/2 + Render::GetTextWidth(ip_string.c_str(), spacing)/2, float(ypos)}, -1, button_active_color.change_a(alpha), {1,2}, shadow_color.change_a(alpha), "|", spacing);
                }

                if (current_menu == &controls_menu && i == int(current_menu->buttons.Size()) - 1 && Keys::AnyDupes())
                    color = button_controls_key;
                Render::Text({float(xoffset), float(ypos)}, alignment, color.change_a(alpha), {1,2}, shadow_color.change_a(alpha), current_menu->buttons[i](), spacing);
            }

            if (fade_in_progress)
            {
                Render::Rect(Game::win_min, Window::Size(), {0,0,0, smoothstep(std::max(fade_timer*1.5f-0.5f,0.f))});
            }
            if (state_load_fade > 0)
            {
                Render::Rect(Game::win_min, Window::Size(), {0,0,0, smoothstep(state_load_fade)});
            }

            Render::main_queue->Send();
            Render::main_queue->DrawTriangles();
        };

        while (1)
        {
            Sys::BeginFrame();
            while (Game::ts->Tick())
            {
                Sys::Tick();
                Tick();
            }
            Render();
            Sys::EndFrame();
            if (Sys::CurrentFunction() != Menu)
                return;
        }
    }
}