#include "sound.h"

#include <cmath>
#include <random>
#include "audio.h"
#include "utils.h"
#include "game.h"

namespace Sound
{
    static float RandomPitch(float range = 1)
    {
        return std::pow(2, (int(Game::rng() % 0x10001u) - 0x10000/2) / float(0x10000/2) * range * 0.75);
    }

    namespace Buffers
    {
        #define S(name,ch) static Audio::Buffer *name;
        SOUND_EFFECTS_LIST
        #undef S
    }

    #define SS(...) __VA_ARGS__
    #define SS_Mono(...) __VA_ARGS__
    #define SS_Stereo(...)
    #define S(name, ch) void name(float v, float p) {Buffers::name->Play(v,p);} \
                        SS##ch(void name(fvec3 pos, float v, float p) {Buffers::name->Play(pos,v,p);}) \
                        void name##_r(float v, float p) {name(v,RandomPitch(p));} \
                        SS##ch(void name##_r(fvec3 pos, float v, float p)) {name(pos,v,RandomPitch(p));}
    SOUND_EFFECTS_LIST
    #undef SS
    #undef SS_Mono
    #undef SS_Stereo
    #undef S

    void LoadFiles()
    {
        ExecuteThisOnce();
        #define S(name,ch) Buffers::name = new Audio::Buffer(Audio::SoundData::FromWAV##ch("assets/" #name ".wav"));
        SOUND_EFFECTS_LIST
        #undef S
    }
}