#ifndef SOUND_H_INCLUDED
#define SOUND_H_INCLUDED

#include "math.h"

namespace Sound
{
    void LoadFiles();

    #define SOUND_EFFECTS_LIST \
        S(click,) \
        S(click_ok,) \
        S(click_error,) \

    #define S(name, ch) void name(float v = 1, float p = 1); \
                        void name(fvec3 pos, float v = 1, float p = 1); \
                        void name##_r(float v = 1, float p = 1); \
                        void name##_r(fvec3 pos, float v = 1, float p = 1);
    SOUND_EFFECTS_LIST
    #undef S
}

#endif