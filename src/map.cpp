#include "map.h"

#include <algorithm>
#include <fstream>
#include <cmath>
#include <iomanip>

#include "keys.h"
#include "system.h"

Map::Map(const char *name)
{
    std::ifstream in(Jo("maps/", name, ".pmap"));
    auto Err = [&](const char *txt){Sys::Error(Jo("Can't parse map `", name, "`: ", txt));};
    auto ErrIfStreamFailure = [&](const char *txt){if (!in) Err(txt);};
    ErrIfStreamFailure("Unable to open file.");
    int len;
    in >> len;
    if (!in || len <= 0)
        Err("Unable to parse size.");
    Utils::Array<VertexFormat> mesh_data(len*3);
    triangles.Alloc(len);
    for (int i = 0; i < len; i++)
    {
        fvec3 in_color;
        fvec2 in_a, in_b, in_c;
        in >> in_color.x >> in_color.y >> in_color.z;
        in >> in_a.x >> in_a.y;
        in >> in_b.x >> in_b.y;
        in >> in_c.x >> in_c.y;

        ErrIfStreamFailure(Jo("Unable to read triangle #", i));

        // This fixed triangle winding to CCW.
        if ((in_b-in_a).x * (in_c-in_a).y - (in_b-in_a).y * (in_c-in_a).x < 0)
            std::swap(in_b, in_c);

        triangles[i].x = in_a;
        triangles[i].y = in_b;
        triangles[i].z = in_c;
        static const float s = std::sqrt(0.5);
        fmat2 rot(0,1,-1,0);
        mesh_data[i*3+0] = {in_a, in_color,                       (((in_c-in_b).norm() /mul/ rot).to_vec3().change_z(1)*s).to_vec4().change_w(1), {0,0,0,0}, {0,0,0,0}};
        mesh_data[i*3+1] = {in_b, in_color,            {0,0,0,0}, (((in_a-in_c).norm() /mul/ rot).to_vec3().change_z(1)*s).to_vec4().change_w(1), {0,0,0,0}};
        mesh_data[i*3+2] = {in_c, in_color, {0,0,0,0}, {0,0,0,0}, (((in_b-in_a).norm() /mul/ rot).to_vec3().change_z(1)*s).to_vec4().change_w(1)};
    }
    mesh.NewData(mesh_data);
}

float Map::TriangleDistance(const fmat3x2 &triangle, fvec2 point, fvec2 *normal) // Normal is not normalized, it's multiplied by distance.
{
    fvec2 out_norm = point - triangle.x;
    float ret = out_norm.len_sqr();
    float tmp_d;
    fvec2 tmp_n;
    if ((tmp_d = (tmp_n = (point - triangle.y)).len_sqr()) < ret)
    {
        ret = tmp_d;
        out_norm = tmp_n;
    }
    if ((tmp_d = (tmp_n = (point - triangle.z)).len_sqr()) < ret)
    {
        ret = tmp_d;
        out_norm = tmp_n;
    }
    ret = std::sqrt(ret);

    for (int i = 0; i < 3; i++)
    {
        const fvec2 &a = triangle[i];
        const fvec2 &b = triangle[(i+1)%3];
        if ((point - a) /dot/ (b - a) < 0 || (point - b) /dot/ (a - b) < 0)
            continue;
        tmp_d = (point - a) /cross/ (b - a).norm();
        if (std::abs(tmp_d) < std::abs(ret))
        {
            ret = tmp_d;
            out_norm = (b - a) /mul/ fmat2(0,1,-1,0);
        }
    }
    *normal = out_norm;
    return ret;
}

EnvData Map::Examine(fvec2 pos, float rad) const
{
    static constexpr float max_dist = 100500000;
    EnvData ret;
    switch (triangles.Size())
    {
      case 0:
        ret.hit = 0;
        ret.second_hit = 0;
        ret.depth = -max_dist;
        ret.second_depth = -max_dist;
        ret.normal = {0,1};
        ret.second_normal = {1,0};
        return ret;
      case 1:
        ret.second_hit =  0;
        ret.second_depth = -max_dist;
        ret.second_normal = {1,0};
        ret.depth = rad - TriangleDistance(triangles[0], pos, &ret.normal);
        ret.hit = (ret.depth > 0);
        return ret;
    }

    ret.depth = TriangleDistance(triangles[0], pos, &ret.normal),
    ret.second_depth = TriangleDistance(triangles[1], pos, &ret.second_normal);
    if (ret.depth > ret.second_depth)
    {
        std::swap(ret.depth, ret.second_depth);
        std::swap(ret.normal, ret.second_normal);
    }
    for (std::size_t i = 2; i < triangles.Size(); i++) // Note the 2.
    {
        fvec2 tmpn;
        float tmpd = TriangleDistance(triangles[i], pos, &tmpn);
        if (tmpd < ret.second_depth)
        {
            if (tmpd < ret.depth)
            {
                ret.second_depth = ret.depth;
                ret.depth = tmpd;
                ret.second_normal = ret.normal;
                ret.normal = tmpn;
            }
            else
            {
                ret.second_depth = tmpd;
                ret.second_normal = tmpn;
            }
        }
    }

    ret.depth = rad - ret.depth;
    ret.hit = (ret.depth > 0);
    ret.second_depth = rad - ret.second_depth;
    ret.second_hit = (ret.second_depth > 0);
    ret.normal = ret.normal.norm();
    ret.second_normal = ret.second_normal.norm();
    return ret;
}

void Map::Tick()
{
    static constexpr float epsilon = 0.00001, gravity_vector_resistance = 0.75;

    auto ClampVel = [](fvec2 vel, const EnvData &data, float bounciness) -> fvec2
    {
        float vel_a = vel /dot/ data.normal;
        float vel_b = vel /dot/ data.second_normal;

        bool clamp_a = (vel_a < 0);
        bool clamp_b = data.second_hit && (vel_b < 0);

        if (!clamp_a && !clamp_b)
            return vel;

        if (clamp_a && clamp_b)
        {
            bool approaching_b = ((vel - data.normal * vel_a) /dot/ data.second_normal < 0);
            bool approaching_a = ((vel - data.second_normal * vel_b) /dot/ data.normal < 0);

            if (approaching_a && approaching_b)
            {
                if ((data.normal - data.second_normal).len_sqr() < epsilon)
                    approaching_a = 0;
                else
                    return {0,0};
            }
            if (approaching_a)
            {
                clamp_a = 1;
                clamp_b = 0;
            }
            else if (approaching_b)
            {
                clamp_a = 0;
                clamp_b = 1;
            }
            else
                return vel; // Should never happen, but it doesn't matter to much if it will.
        }

        if (clamp_a)
            return vel - data.normal.norm() * vel_a * (1 + bounciness);
        // if clamp_b
        return vel - data.second_normal.norm() * vel_b * (1 + bounciness);
    };
    auto PushOut = [](fvec2 pos, const EnvData &data) -> fvec2
    {
        float new_second_depth = data.second_depth - (data.normal * data.depth) /dot/ data.second_normal;
        pos += data.normal * data.depth;
        if ((data.second_hit || new_second_depth > 0) && (data.normal - data.second_normal).len_sqr() > epsilon)
        {
            fvec2 surface = data.normal /mul/ fmat2(0,-1,1,0);
            pos += surface * surface /dot/ data.second_normal * new_second_depth;
        }
        return pos;
    };

    for (auto it = particles.begin(); it != particles.end(); /*nothing*/)
    {
        it->prev_pos = it->pos;
        it->grav = (it->grav * gravity_vector_resistance - it->env.normal * (1 - gravity_vector_resistance)).norm();
        it->vel += it->acc + it->grav * gravity * it->gravity_factor / ipow(gravity_distance_factor * (std::max(it->radius, 0.f) - it->env.depth + gravity_common_distance), 2);
        it->vel *= it->vel_fac;
        it->pos += it->vel;
        it->angle += it->angular_vel;
        it->angular_vel *= it->angular_vel_fac;
        it->alpha += it->alpha_vel;
        if (it->life == 0)
        {
            it = particles.erase(it);
            continue;
        }
        if (it->life > 0)
            it->life--;

        it->env = Examine(it->pos, std::max(it->radius, 0.f));

        if (it->radius > 0 && it->env.hit)
        {
            it->vel = ClampVel(it->vel, it->env, it->bounciness);
            it->pos = PushOut(it->pos, it->env);
            it->angular_vel = it->env.normal /cross/ (it->pos - it->prev_pos) / it->radius;
        }

        it++;
    }

    for (auto it = players.begin(); it != players.end(); /*nothing*/)
    {
        it->grav = (it->grav * gravity_vector_resistance - it->env.normal * (1 - gravity_vector_resistance)).norm();
        it->vel += it->grav * gravity * Player::gravity_factor / ipow(gravity_distance_factor * (Player::radius - it->env.depth + gravity_common_distance), 2);
        if (it->controller == Player::Controller::local_player)
        {
            fvec2 x_axis = it->env.normal /mul/ fmat2(0,-1,1,0);

            float hc = Keys::Down::right() - Keys::Down::left();
            if (!it->env.hit)
                hc *= 0.1;

            float x_vel = x_axis /dot/ it->vel;
            it->vel -= x_axis * x_vel;
            x_vel = clamp(x_vel * (it->env.hit ? Player::ground_vel_factor : 1) + hc * Player::walk_acc, -Player::walk_vel_lim, Player::walk_vel_lim);
            it->vel += x_axis * x_vel;

            if (Keys::Pressed::jump() && it->env.hit)
            {
                it->vel += it->env.normal * 5;
            }
        }
        it->vel *= Player::vel_factor;
        it->pos += it->vel;
        it->env = Examine(it->pos, 30);
        if (it->env.hit)
        {
            it->vel = ClampVel(it->vel, it->env, 0);
            it->pos = PushOut(it->pos, it->env);
        }

        it++;
    }
}