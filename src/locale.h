#ifndef LOCALE_H_INCLUDED
#define LOCALE_H_INCLUDED

namespace Locale
{
    #define LOCALE_VARS_LIST \
        L( menu_singleplayer   , "Practice"          , "Тренировка"                 ) \
        L( menu_multiplayer    , "Multiplayer"       , "Сетевая игра"               ) \
        L( menu_settings       , "Settings"          , "Настройки"                  ) \
        L( menu_quit           , "Quit"              , "Выход"                      ) \
        L( menu_language       , "Language: EN"      , "Язык: RU"                   ) \
        L( menu_controls       , "Controls"          , "Управление"                 ) \
        L( menu_reset_controls , "Reset"             , "Сбросить"                   ) \
        L( menu_connect        , "Connect"           , "Подключиться"               ) \
        L( menu_host           , "Host"              , "Создать сервер"             ) \
        L( menu_join           , "Join"              , "Подключение"                ) \
        L( menu_ip             , "IP: "              , "IP: "                       ) \
        L( menu_back           , "Back"              , "Назад"                      ) \
        L( keys_left           , "Move left: "       , "Идти влево: "               ) \
        L( keys_right          , "Move right: "      , "Идти вправо: "              ) \
        L( keys_jump           , "Jump: "            , "Прыжок: "                   ) \

    bool English();
    bool Russian();
    void UseEnglish();
    void UseRussian();

    #define L(name, eng, rus) inline const char *name() {if (English()) return eng; else return rus;}
    LOCALE_VARS_LIST
    #undef LOCALE_VARS_LIST

    void SaveToFile();
    void LoadFromFile();
}

#endif