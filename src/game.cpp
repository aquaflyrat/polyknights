#include "game.h"
#include "master.h"
#include "state_menu.h"
#include "state_game.h"
#include "locale.h"
#include "keys.h"
#include "sound.h"
#include "map.h"

namespace Game
{
    std::mt19937 rng(std::random_device{}());

    ivec2 win_min, win_max;

    Utils::TickStabilizer *ts;

    void Boot()
    {
        MarkLocation("Boot");

        Render::Initialize();

        ts = new Utils::TickStabilizer(60);

        //Input::ShowMouse(0);
        Input::SetMouseMapping([](ivec2 in)->ivec2{return in - Window::Size()/2;}, [](ivec2 in)->ivec2{return in + Window::Size()/2;});

        Locale::LoadFromFile();
        Keys::LoadFromFile();
        Sound::LoadFiles();

        Sys::SetCurrentFunction(States::Menu);
        //Sys::SetCurrentFunction(States::Game);
    }
}

void PreInit()
{
    Sys::SetCurrentFunction(Game::Boot);
    Window::Init::Name("Poly Knights");
    Window::Init::Resizable(1);
    Window::Init::OpenGL::MSAA(16);
    Sys::Config::ApplicationName("PolyKnights");
}

void Resize()
{
    Graphics::ViewportFullscreen();
    Game::win_min = -Window::Size() / 2;
    Game::win_max = (Window::Size() + 1) / 2;
    Render::main_shader->SetUniform<fmat4>(0, fmat4::ortho({(float)Game::win_min.x, (float)Game::win_max.y}, {(float)Game::win_max.x, (float)Game::win_min.y}, -1, 1));
    Render::menu_shader->SetUniform<fvec2>(1, Window::Size());
    fvec2 map_scr_sz;
    if (Window::Size().ratio() >= Map::ref_screen_size.ratio())
        map_scr_sz = {float(Map::ref_screen_size.y * Window::Size().ratio()), Map::ref_screen_size.y};
    else
        map_scr_sz = {Map::ref_screen_size.x, float(Map::ref_screen_size.x / Window::Size().ratio())};
    fmat4 map_proj_mat = fmat4::ortho({-map_scr_sz.x/2,map_scr_sz.y/2}, {map_scr_sz.x/2,-map_scr_sz.y/2}, -1, 1);
    Render::map_shader->SetUniform<fmat4>(0, map_proj_mat);
    Render::world_shader->SetUniform<fmat4>(0, map_proj_mat);
}