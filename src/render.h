#ifndef RENDER_H_INCLUDED
#define RENDER_H_INCLUDED

#include "graphics.h"

namespace Render
{
    void Initialize();

    namespace Layouts
    {
        //                                  pos    tex    color  factors
        using Main = Graphics::VertexFormat<fvec2, fvec2, fvec4, fvec2>;
        using Menu = Graphics::VertexFormat<fvec2>;
        //                                  pos    color  norm_a norm_b norm_c
        using Map  = Graphics::VertexFormat<fvec2, fvec3, fvec4, fvec4, fvec4>;
    }

    extern Graphics::Shader *main_shader,
                            *menu_shader,
                            *map_shader,
                            *world_shader;

    extern Graphics::Texture *main_texture;
    extern Graphics::FontData main_font_data;
    extern Graphics::RenderQueue<Layouts::Main> *main_queue;

    inline void Rect(fvec2 dst, fvec2 dstsz, fvec2 src, fvec2 srcsz, fmat4 colors, fmat4x2 factors)
    {
        dstsz += dst;
        srcsz += src;
        main_queue->Push4as3x2({{dst  .x,dst  .y},{src  .x,src  .y},colors.x,factors.x},
                               {{dstsz.x,dst  .y},{srcsz.x,src  .y},colors.y,factors.y},
                               {{dstsz.x,dstsz.y},{srcsz.x,srcsz.y},colors.z,factors.z},
                               {{dst  .x,dstsz.y},{src  .x,srcsz.y},colors.w,factors.w});
    }
    inline void RectR(fvec2 dst, fvec2 dstsz, fvec2 src, fvec2 srcsz, fmat4 colors, fmat4x2 factors, float angle, fvec2 center)
    {
        fvec2 fa = dstsz / srcsz;
        fvec2 a = -center*fa;
        fvec2 c = (srcsz-center)*fa;
        fvec2 b(c.x,a.y);
        fvec2 d(a.x,c.y);
        fmat2 rot = fmat2::rotate2D(angle);
        a = rot /mul/ a + dst;
        b = rot /mul/ b + dst;
        c = rot /mul/ c + dst;
        d = rot /mul/ d + dst;
        srcsz += src;
        main_queue->Push4as3x2({a,{src  .x,src  .y},colors.x,factors.x},
                               {b,{srcsz.x,src  .y},colors.y,factors.y},
                               {c,{srcsz.x,srcsz.y},colors.z,factors.z},
                               {d,{src  .x,srcsz.y},colors.w,factors.w});
    }
    inline void Rect(fvec2 dst, fvec2 dstsz, fvec2 src, fvec2 srcsz, fvec4 color, fvec2 factor)
    {
        Rect(dst, dstsz, src, srcsz, {color,color,color,color}, {factor,factor,factor,factor});
    }
    inline void RectR(fvec2 dst, fvec2 dstsz, fvec2 src, fvec2 srcsz, fvec4 color, fvec2 factor, float angle, fvec2 center)
    {
        RectR(dst, dstsz, src, srcsz, {color,color,color,color}, {factor,factor,factor,factor}, angle, center);
    }
    inline void Rect(fvec2 dst, fvec2 dstsz, fvec2 src, fvec4 color, fvec2 factor)
    {
        Rect(dst, dstsz, src, dstsz, color, factor);
    }
    inline void Rect(fvec2 dst, fvec2 dstsz, fvec2 src, float alpha = 1)
    {
        Rect(dst, dstsz, src, {0,0,0,0}, {1,alpha});
    }
    inline void Rect(fvec2 dst, fvec2 dstsz, fvec2 src, fvec2 srcsz, float alpha = 1)
    {
        Rect(dst, dstsz, src, srcsz, {0,0,0,0}, {1,alpha});
    }
    inline void Rect(fvec2 dst, fvec2 dstsz, fvec4 color)
    {
        Rect(dst, dstsz, {0,0}, {0,0}, color, {0,0});
    }

    struct Sprite // Do not add margins to this.
    {
        ivec2 pos, size, center_offset;
    };

    inline void RectR(fvec2 pos, const Sprite &s, float angle, fvec4 color, fvec2 factor)
    {
        RectR(pos, fvec2(s.size-2)/4, s.pos+1, s.size-2, color, factor, angle, fvec2(s.size-2)/2 + s.center_offset);
    }
    inline void Rect(fvec2 pos, const Sprite &s, fvec4 color, fvec2 factor)
    {
        RectR(pos, s, 0, color, factor);
    }
    inline void RectR(fvec2 pos, const Sprite &s, float angle, float alpha = 1)
    {
        RectR(pos, s, angle, {0,0,0,0}, {1,alpha});
    }
    inline void Rect(fvec2 pos, const Sprite &s, float alpha = 1)
    {
        Rect(pos, s, 0, alpha);
    }

    inline float GetTextWidth(const char *txt, float spacing = 0) // Stops at \0 and \n.
    {
        float ret = 0;
        uint16_t c = 0, prev_c;
        while (*txt != '\0' && *txt != '\n')
        {
            prev_c = c;
            c = u8decode(txt, &txt);
            if (!main_font_data.HasGlyph(c))
                continue;
            ret += main_font_data.Advance(c) + main_font_data.Kerning(prev_c, c) + (prev_c ? spacing : 0);
        }
        return ret;
    }

    inline void Text(fvec2 pos, int xalign, fvec4 color, const char *txt, float spacing = 0)
    {
        fvec2 cur_pos = pos;
        cur_pos.x -= GetTextWidth(txt, spacing) * (xalign + 1) / 2;
        uint16_t c = 0, prev_c;
        while (*txt)
        {
            prev_c = c;
            c = u8decode(txt, &txt);
            if (c == '\n')
            {
                cur_pos.x = pos.x - GetTextWidth(txt+1, spacing) * (xalign + 1) / 2;;
                cur_pos.y += main_font_data.Height();
                continue;
            }
            if (!main_font_data.HasGlyph(c))
                continue;
            Rect(fvec2(std::round(cur_pos.x), cur_pos.y) + main_font_data.Offset(c), main_font_data.Size(c), main_font_data.Pos(c), color.change_a(0), {0,color.a});
            cur_pos.x += main_font_data.Advance(c) + main_font_data.Kerning(prev_c, c) + spacing;
        }
    }
    inline void Text(fvec2 pos, int xalign, fvec4 color, fvec2 shadow_offset, fvec4 shadow, const char *txt, float spacing = 0)
    {
        Text(pos+shadow_offset, xalign, shadow, txt, spacing);
        Text(pos, xalign, color, txt, spacing);
    }

    inline void MenuBackground()
    {
        static Graphics::SizedVertexArray<Layouts::Menu> buf({{{-1,-1}},{{1,-1}},{{-1,1}},{{1,-1}},{{-1,1}},{{1,1}}});
        menu_shader->SetUniform<float>(0, fmod(Sys::TickCounter() / 60.0, 100.0));
        buf.DrawTriangles();
    }
}

namespace Sprites
{
    constexpr Render::Sprite player_head{{256+1,0+1}, {280-2,256-2}, {0,0}};
}

#endif