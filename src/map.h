#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include "math.h"
#include "graphics.h"
#include "render.h"
#include <vector>

struct EnvData
{
    bool hit;
    fvec2 normal; // Normalized
    float depth;
    bool second_hit;
    fvec2 second_normal; // Normalized
    float second_depth;
};

struct Player
{
    enum Controller {local_player};

    static constexpr float gravity_factor = 1, radius = 30, vel_factor = 0.995, ground_vel_factor = 0.95, walk_acc = 0.3, walk_vel_lim = 5;

    fvec2 pos, vel, grav;
    Controller controller;

    EnvData env;
};

class Map
{
  public:
    using VertexFormat = Render::Layouts::Map;
    static constexpr fvec2 ref_screen_size{1280,960};

    static constexpr float gravity = 0.25, gravity_common_distance = 100, gravity_distance_factor = 0.01;

  private:
    Utils::Array<fmat3x2> triangles;
    Graphics::SizedVertexArray<VertexFormat> mesh;

    struct Particle
    {
        const Render::Sprite *s;
        EnvData env;
        fvec2 pos, prev_pos, vel, acc, grav;
        float vel_fac;
        float angle, angular_vel, angular_vel_fac;
        float alpha, alpha_vel;
        int life;
        float radius; // If `radius > 0`, then physics calculations are performed
        float bounciness; // 0 means no bounce jumps, 1 means full height bounce jumps.
        float gravity_factor;
    };
    std::vector<Particle> particles;

    static float TriangleDistance(const fmat3x2 &triangle, fvec2 point, fvec2 *normal); // Normal is not normalized, it's multiplied by distance.
    EnvData Examine(fvec2 pos, float rad) const;

  public:
    std::vector<Player> players;

    Map() {}
    Map(const char *name);

    void Tick();

    void AddParticle(const Render::Sprite &s, fvec2 pos, fvec2 vel, fvec2 acc, float vel_fac, float angle, float ang_vel, float ang_vel_fac, float alpha, float alpha_vel, int life, float rad, float bounciness, float gravity)
    {
        particles.push_back({&s, {}, pos, pos, vel, acc, {}, vel_fac, angle, ang_vel, ang_vel_fac, alpha, alpha_vel, life, rad, bounciness, gravity});
    }

    void Draw(fvec2 pos, float angle = 0) const
    {
        fmat4 view = fmat4::rotate_with_normalized_axis({0,0,1}, angle) /mul/ fmat4::translate({-pos.x, -pos.y, 0});
        Render::map_shader->SetUniform<fmat4>(1, view);
        Render::map_shader->Use();
        mesh.DrawTriangles();
        Render::world_shader->SetUniform<fmat4>(1, view);
        Render::world_shader->Use();
        Render::main_queue->Clear();
        for (auto it : particles)
        {
            Render::RectR(it.pos, *it.s, it.angle, clamp(it.alpha, 0, 1));
        }
        for (auto it : players)
        {
            Render::RectR(it.pos, Sprites::player_head, std::atan2(-it.grav.x, it.grav.y), 1);
        }
        Render::main_queue->Send();
        Render::main_queue->DrawTriangles();
    }
};

#endif