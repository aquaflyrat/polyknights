#include "state_game.h"
#include "master.h"
#include "map.h"

namespace States
{
    void Game()
    {
        Render::main_texture->LinearInterpolation(1);
        Map map("1");

        Graphics::SetClearColor01({0.5,1,1,1});
        fvec2 pos(300,300);
        float angle = 0;

        map.players.push_back({{300,0}, {0,0}, {}, Player::Controller::local_player, {}});
        //map.AddParticle(Sprites::player_head, {300, -50}, {-2,0}, {0,0}, 0.995, 0, 0, 0.98, 1, 0, -1, 30, 0.7, 1);

        auto Tick = [&]()
        {
            /*
            int hc = Input::KeyDown(Input::Key<'D'>()) - Input::KeyDown(Input::Key<'A'>());
            int vc = Input::KeyDown(Input::Key<'S'>()) - Input::KeyDown(Input::Key<'W'>());
            int ac = Input::KeyDown(Input::Key<'Q'>()) - Input::KeyDown(Input::Key<'E'>());
            pos += 10 * hc * fvec2(std::cos(-angle), std::sin(-angle));
            pos += 10 * vc * fvec2(-std::sin(-angle), std::cos(-angle));
            angle += ac * 0.013;
            */
            pos = map.players[0].pos;
            float angle_diff = -std::atan2(-map.players[0].grav.x, map.players[0].grav.y) - angle;
            while (angle_diff < -pi<float>())
                angle_diff += pi<float>() * 2;
            while (angle_diff > pi<float>())
                angle_diff -= pi<float>() * 2;
            angle += angle_diff * 0.015;

            if (!std::isnormal(pos.x))
            {
                std::cout << map.players[0].env.hit << map.players[0].env.second_hit << ' ' << map.players[0].env.depth << '\t' << map.players[0].env.normal << '\t' << map.players[0].env.second_depth << '\t' << map.players[0].env.second_normal;
                std::system("pause");
                Sys::Exit();
            }

            map.Tick();
            //fvec2 dir; std::cout << map.TriangleDistance(map.triangles[0], Input::MousePos() / fvec2(Window::Size()) * Map::ref_screen_size + pos, &dir) << '\t' << dir << '\n';
        };
        auto Render = [&]()
        {
            map.Draw(pos, angle);
        };

        while (1)
        {
            Sys::BeginFrame();
            while (Game::ts->Tick())
            {
                Sys::Tick();
                Tick();
            }
            Render();
            Sys::EndFrame();
            if (Sys::CurrentFunction() != Game)
                return;
        }
    }
}